package ru.siburtestapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.siburtestapp.adapters.DataListAdapter;
import ru.siburtestapp.model.CombinedFilckrAndGithub;

public class MainActivity extends AppCompatActivity implements IMainView {

    @BindView(R.id.btnLoad) Button btnLoad;
    @BindView(R.id.pBarDataLoad) ProgressBar pBarDataLoad;
    @BindView(R.id.lvDataList) ListView lvDataList;
    @BindView(R.id.tvEmptyList) TextView tvEmptyList;

    private MainPresenter mainPresenter;
    private DataListAdapter dataListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mainPresenter = new MainPresenter(this);

        dataListAdapter = new DataListAdapter(this);

        lvDataList.setEmptyView(tvEmptyList);

        lvDataList.setAdapter(dataListAdapter);

        lvDataList.setOnItemClickListener((adapterView, view, i, l) -> loadData());
    }

    private void loadData(){

        if (dataListAdapter != null)
            dataListAdapter.clearList();

        if (mainPresenter != null)
            mainPresenter.loadData();
    }

    @OnClick(R.id.btnLoad)
    public void submit(View view) {
        loadData();
    }

    @Override
    protected void onStop() {

        if (mainPresenter != null)
            mainPresenter.onStop();

        super.onStop();

    }

    @Override
    public void showList(List<CombinedFilckrAndGithub> combinedFilckrAndGithubList) {

        if (dataListAdapter != null)
            dataListAdapter.setCombinedFilckrAndGithubList(combinedFilckrAndGithubList);
    }

    @Override
    public void showError() {
        Toast.makeText(this, getString(R.string.error_massage), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        pBarDataLoad.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pBarDataLoad.setVisibility(View.GONE);
    }
}
