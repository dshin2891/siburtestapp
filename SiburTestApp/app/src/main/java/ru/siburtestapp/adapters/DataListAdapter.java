package ru.siburtestapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.siburtestapp.R;
import ru.siburtestapp.model.CombinedFilckrAndGithub;

//Класс адаптер для списка из объединенных данных с github и Flickr

public class DataListAdapter extends ArrayAdapter<CombinedFilckrAndGithub> {

    private Context context;
    private List<CombinedFilckrAndGithub> combinedFilckrAndGithubList = new ArrayList<>();

    public DataListAdapter(Context context){
        super(context, R.layout.data_list_item);
        this.context = context;
        }

    public void setCombinedFilckrAndGithubList(List<CombinedFilckrAndGithub> combinedFilckrAndGithubList) {
        this.combinedFilckrAndGithubList = combinedFilckrAndGithubList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return combinedFilckrAndGithubList.size();
    }

    CombinedFilckrAndGithub getCombinedFilckrAndGithub(int position) {
        return combinedFilckrAndGithubList.get(position);
    }

    public void clearList(){
        this.combinedFilckrAndGithubList.clear();
        this.notifyDataSetChanged();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(context).inflate(R.layout.data_list_item, parent, false);

        TextView tvGithubRepositoryName = (TextView) convertView.findViewById(R.id.tvGithubRepositoryName);
        ImageView ivFlickrImage = (ImageView) convertView.findViewById(R.id.ivFlickrImage);

        CombinedFilckrAndGithub combinedFilckrAndGithub = getCombinedFilckrAndGithub(position);

//        Проверяем объект содержит url для картинки с Flickr, если да, то
//        загружаем картинку, если нет, то вставляем название репозитория

        if (combinedFilckrAndGithub.isImageUrl()){
            tvGithubRepositoryName.setVisibility(View.GONE);
            Picasso.get().load(combinedFilckrAndGithub.getValue()).into(ivFlickrImage);
        }else{
            ivFlickrImage.setVisibility(View.GONE);
            tvGithubRepositoryName.setText(combinedFilckrAndGithub.getValue());
        }

        return convertView;
    }
}
