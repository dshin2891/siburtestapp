package ru.siburtestapp.model;

//Вспомогательный класс для объединения данных с github и Flickr
public class CombinedFilckrAndGithub {

    public CombinedFilckrAndGithub(){}

//    поле показывающее содержит объект url картинки с Flickr
//    или название репозитория с github
//    если true значит поле value это url картинки
//    если false, значит название репозитория с github
    private boolean isImageUrl;

//    Поле содержащее либо url картинки с Flickr, либо название репозитория с github
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isImageUrl() {
        return isImageUrl;
    }

    public void setIsImageUrl(boolean imageUrl) {
        isImageUrl = imageUrl;
    }
}
