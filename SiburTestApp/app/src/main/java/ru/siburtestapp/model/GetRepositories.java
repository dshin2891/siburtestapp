package ru.siburtestapp.model;

import com.apollographql.apollo.api.Field;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Query;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import com.apollographql.apollo.api.internal.Utils;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Generated("Apollo GraphQL")
public final class GetRepositories implements Query<GetRepositories.Data, GetRepositories.Data, GetRepositories.Variables> {
  public static final String OPERATION_DEFINITION = "query GetRepositories($user_login: String!, $repositories_count: Int!) {\n"
      + "  user(login: $user_login) {\n"
      + "    __typename\n"
      + "    repositories(first: $repositories_count) {\n"
      + "      __typename\n"
      + "      nodes {\n"
      + "        __typename\n"
      + "        name\n"
      + "      }\n"
      + "    }\n"
      + "  }\n"
      + "}";

  public static final String QUERY_DOCUMENT = OPERATION_DEFINITION;

  private static final OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "GetRepositories";
    }
  };

  private final GetRepositories.Variables variables;

  public GetRepositories(@Nonnull String user_login, int repositories_count) {
    Utils.checkNotNull(user_login, "user_login == null");
    variables = new GetRepositories.Variables(user_login, repositories_count);
  }

  @Override
  public String queryDocument() {
    return QUERY_DOCUMENT;
  }

  @Override
  public GetRepositories.Data wrapData(GetRepositories.Data data) {
    return data;
  }

  @Override
  public GetRepositories.Variables variables() {
    return variables;
  }

  @Override
  public ResponseFieldMapper<Data> responseFieldMapper() {
    return new Data.Mapper();
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public OperationName name() {
    return OPERATION_NAME;
  }

  public static final class Variables extends Operation.Variables {
    private final @Nonnull String user_login;

    private final int repositories_count;

    private final transient Map<String, Object> valueMap = new LinkedHashMap<>();

    Variables(@Nonnull String user_login, int repositories_count) {
      this.user_login = user_login;
      this.repositories_count = repositories_count;
      this.valueMap.put("user_login", user_login);
      this.valueMap.put("repositories_count", repositories_count);
    }

    public @Nonnull String user_login() {
      return user_login;
    }

    public int repositories_count() {
      return repositories_count;
    }

    @Override
    public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
  }

  public static final class Builder {
    private @Nonnull String user_login;

    private int repositories_count;

    Builder() {
    }

    public Builder user_login(@Nonnull String user_login) {
      this.user_login = user_login;
      return this;
    }

    public Builder repositories_count(int repositories_count) {
      this.repositories_count = repositories_count;
      return this;
    }

    public GetRepositories build() {
      if (user_login == null) throw new IllegalStateException("user_login can't be null");
      return new GetRepositories(user_login, repositories_count);
    }
  }

  public static class Data implements Operation.Data {
    private final @Nullable User user;

    private volatile String $toString;

    private volatile int $hashCode;

    private volatile boolean $hashCodeMemoized;

    public Data(@Nullable User user) {
      this.user = user;
    }

    /**
     * Lookup a user by login.
     */
    public @Nullable User user() {
      return this.user;
    }

    @Override
    public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "user=" + user
          + "}";
      }
      return $toString;
    }

    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.user == null) ? (that.user == null) : this.user.equals(that.user));
      }
      return false;
    }

    @Override
    public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (user == null) ? 0 : user.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      return $hashCode;
    }

    public static final class Mapper implements ResponseFieldMapper<Data> {
      final User.Mapper userFieldMapper = new User.Mapper();

      final Field[] fields = {
        Field.forObject("user", "user", new UnmodifiableMapBuilder<String, Object>(1)
          .put("login", new UnmodifiableMapBuilder<String, Object>(2)
            .put("kind", "Variable")
            .put("variableName", "user_login")
          .build())
        .build(), true, new Field.ObjectReader<User>() {
          @Override public User read(final ResponseReader reader) throws IOException {
            return userFieldMapper.map(reader);
          }
        })
      };

      @Override
      public Data map(ResponseReader reader) throws IOException {
        final User user = reader.read(fields[0]);
        return new Data(user);
      }
    }
  }

  public static class User {
    private final @Nonnull String __typename;

    private final @Nonnull Repositories repositories;

    private volatile String $toString;

    private volatile int $hashCode;

    private volatile boolean $hashCodeMemoized;

    public User(@Nonnull String __typename, @Nonnull Repositories repositories) {
      this.__typename = __typename;
      this.repositories = repositories;
    }

    public @Nonnull String __typename() {
      return this.__typename;
    }

    /**
     * A list of repositories that the user owns.
     */
    public @Nonnull Repositories repositories() {
      return this.repositories;
    }

    @Override
    public String toString() {
      if ($toString == null) {
        $toString = "User{"
          + "__typename=" + __typename + ", "
          + "repositories=" + repositories
          + "}";
      }
      return $toString;
    }

    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof User) {
        User that = (User) o;
        return this.__typename.equals(that.__typename)
         && this.repositories.equals(that.repositories);
      }
      return false;
    }

    @Override
    public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= __typename.hashCode();
        h *= 1000003;
        h ^= repositories.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      return $hashCode;
    }

    public static final class Mapper implements ResponseFieldMapper<User> {
      final Repositories.Mapper repositoriesFieldMapper = new Repositories.Mapper();

      final Field[] fields = {
        Field.forString("__typename", "__typename", null, false),
        Field.forObject("repositories", "repositories", new UnmodifiableMapBuilder<String, Object>(1)
          .put("first", new UnmodifiableMapBuilder<String, Object>(2)
            .put("kind", "Variable")
            .put("variableName", "repositories_count")
          .build())
        .build(), false, new Field.ObjectReader<Repositories>() {
          @Override public Repositories read(final ResponseReader reader) throws IOException {
            return repositoriesFieldMapper.map(reader);
          }
        })
      };

      @Override
      public User map(ResponseReader reader) throws IOException {
        final String __typename = reader.read(fields[0]);
        final Repositories repositories = reader.read(fields[1]);
        return new User(__typename, repositories);
      }
    }
  }

  public static class Repositories {
    private final @Nonnull String __typename;

    private final @Nullable List<Node> nodes;

    private volatile String $toString;

    private volatile int $hashCode;

    private volatile boolean $hashCodeMemoized;

    public Repositories(@Nonnull String __typename, @Nullable List<Node> nodes) {
      this.__typename = __typename;
      this.nodes = nodes;
    }

    public @Nonnull String __typename() {
      return this.__typename;
    }

    /**
     * A list of nodes.
     */
    public @Nullable List<Node> nodes() {
      return this.nodes;
    }

    @Override
    public String toString() {
      if ($toString == null) {
        $toString = "Repositories{"
          + "__typename=" + __typename + ", "
          + "nodes=" + nodes
          + "}";
      }
      return $toString;
    }

    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Repositories) {
        Repositories that = (Repositories) o;
        return this.__typename.equals(that.__typename)
         && ((this.nodes == null) ? (that.nodes == null) : this.nodes.equals(that.nodes));
      }
      return false;
    }

    @Override
    public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= __typename.hashCode();
        h *= 1000003;
        h ^= (nodes == null) ? 0 : nodes.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      return $hashCode;
    }

    public static final class Mapper implements ResponseFieldMapper<Repositories> {
      final Node.Mapper nodeFieldMapper = new Node.Mapper();

      final Field[] fields = {
        Field.forString("__typename", "__typename", null, false),
        Field.forList("nodes", "nodes", null, true, new Field.ObjectReader<Node>() {
          @Override public Node read(final ResponseReader reader) throws IOException {
            return nodeFieldMapper.map(reader);
          }
        })
      };

      @Override
      public Repositories map(ResponseReader reader) throws IOException {
        final String __typename = reader.read(fields[0]);
        final List<Node> nodes = reader.read(fields[1]);
        return new Repositories(__typename, nodes);
      }
    }
  }

  public static class Node {
    private final @Nonnull String __typename;

    private final @Nonnull String name;

    private volatile String $toString;

    private volatile int $hashCode;

    private volatile boolean $hashCodeMemoized;

    public Node(@Nonnull String __typename, @Nonnull String name) {
      this.__typename = __typename;
      this.name = name;
    }

    public @Nonnull String __typename() {
      return this.__typename;
    }

    /**
     * The name of the repository.
     */
    public @Nonnull String name() {
      return this.name;
    }

    @Override
    public String toString() {
      if ($toString == null) {
        $toString = "Node{"
          + "__typename=" + __typename + ", "
          + "name=" + name
          + "}";
      }
      return $toString;
    }

    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Node) {
        Node that = (Node) o;
        return this.__typename.equals(that.__typename)
         && this.name.equals(that.name);
      }
      return false;
    }

    @Override
    public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= __typename.hashCode();
        h *= 1000003;
        h ^= name.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      return $hashCode;
    }

    public static final class Mapper implements ResponseFieldMapper<Node> {
      final Field[] fields = {
        Field.forString("__typename", "__typename", null, false),
        Field.forString("name", "name", null, false)
      };

      @Override
      public Node map(ResponseReader reader) throws IOException {
        final String __typename = reader.read(fields[0]);
        final String name = reader.read(fields[1]);
        return new Node(__typename, name);
      }
    }
  }
}
