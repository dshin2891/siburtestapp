package ru.siburtestapp.webApi;

import com.apollographql.apollo.ApolloClient;

import okhttp3.OkHttpClient;
import okhttp3.Request;

//Класс сборщик для работы с Api github
public class GithubApolloApiFactory {

    private static final String BASE_URL = "https://api.github.com/graphql";
    private static final String AUTH_TOKEN = "f78b6a31d242b8f7f1a9c706a34998640f9d6b9f";

    private static OkHttpClient GithubOkHttpClient(){

        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();

        httpClient.interceptors().add(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Authorization"
                            , "Bearer " + AUTH_TOKEN)
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        });

        return httpClient.build();
    }

    public static ApolloClient setupGithubApolloClient(){
        return ApolloClient.builder()
                .serverUrl(BASE_URL)
                .okHttpClient(GithubOkHttpClient())
                .build();
    }
}
