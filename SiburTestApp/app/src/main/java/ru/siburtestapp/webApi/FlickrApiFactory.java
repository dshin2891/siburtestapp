package ru.siburtestapp.webApi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

//Класс сборщик для работы с Api Flickr
public class FlickrApiFactory {

    private static final String BASE_URL = "https://api.flickr.com";

    public static FlickrApiInterface getFlickrApiInterface(){

        Retrofit.Builder builder = new Retrofit.Builder().
                baseUrl(BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        return builder.build().create(FlickrApiInterface.class);
    }
}
