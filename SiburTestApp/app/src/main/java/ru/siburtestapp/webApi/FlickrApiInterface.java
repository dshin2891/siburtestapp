package ru.siburtestapp.webApi;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.siburtestapp.model.FlickrDataModel;

//Интерфейс, содержащий методы для работы с API Flickr
public interface FlickrApiInterface {

    //Метод get запроса к API Flickr для получения данных по фотографиям
@GET("/services/rest")
    Observable<FlickrDataModel> getFlickrData(@Query("method") String method
        , @Query("api_key") String apiKey
        , @Query("tags")String tag
        , @Query("per_page")String countPerPage
        , @Query("format") String format
        , @Query("nojsoncallback") String nojsoncallback);
}
