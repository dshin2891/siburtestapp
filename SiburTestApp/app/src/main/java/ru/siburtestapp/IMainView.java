package ru.siburtestapp;

import java.util.List;

import ru.siburtestapp.model.CombinedFilckrAndGithub;


public interface IMainView {

    void showList(List<CombinedFilckrAndGithub> combinedFilckrAndGithubList);

    void showError();

    void showLoading();

    void hideLoading();
}
