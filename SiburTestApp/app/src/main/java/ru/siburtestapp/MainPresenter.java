package ru.siburtestapp;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.rx2.Rx2Apollo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import ru.siburtestapp.model.CombinedFilckrAndGithub;
import ru.siburtestapp.model.FlickrDataModel;
import ru.siburtestapp.model.GetRepositories;
import ru.siburtestapp.model.Photo;
import ru.siburtestapp.webApi.FlickrApiFactory;
import ru.siburtestapp.webApi.FlickrApiInterface;
import ru.siburtestapp.webApi.GithubApolloApiFactory;

// Класс презентер, содержащий основную логику и загрузку данныхс Api
public class MainPresenter {

    //поле метода для получения фотографий по Api Flickr
    private static final String FLICKR_API_METHOD = "flickr.photos.search";
    //ключ api для запросов к серверу Flickr
    private static final String FLICKR_API_KEY = "92083d3cb4c9fec46846efcb2467dfb3";
    //поле необходимое для поиска картинок по api Flickr по заданной тематике, в данном случае animal
    private static final String FLICKR_TAG = "animal";
    //поле в котором задается формат возвращаемого значения json
    private static final String FLICKR_FORMAT = "json";
    //поле ограничивающее количество возвращаемых объектов photo в запросе
    private static final int FLICKR_COUNT_PER_PAGE = 50;
    private static final int FLICKR_NOJSONCALLBACK = 1;

    // случайный никнейм пользователя github. Загружаем названия репозиторев данного пользователя
    private static final String GITHUB_NICKNAME = "andrew";
    // количество репозиториев для загрузки.В данном случае первые 50
    private static final int GITHUB_REPOSITORY_COUNT = 50;

    private final CompositeDisposable disposables = new CompositeDisposable();

    private IMainView iMainView;

    public MainPresenter(IMainView iMainView){
        this.iMainView = iMainView;
    }

    public void loadData(){
        ApolloClient apolloClient = GithubApolloApiFactory.setupGithubApolloClient();

        ApolloCall<GetRepositories.Data> apolloCall = apolloClient.query(GetRepositories
                .builder()
                .user_login(GITHUB_NICKNAME)
                .repositories_count(GITHUB_REPOSITORY_COUNT)
                .build());
        // Observable для загрузки данных с github
        Observable<Response<GetRepositories.Data>> observable = Rx2Apollo.from(apolloCall);

        FlickrApiInterface flickrApiInterface = FlickrApiFactory.getFlickrApiInterface();

        //Observable для загрузки данных с Flickr
        Observable<FlickrDataModel> flickrDataModelObservable = flickrApiInterface.getFlickrData(FLICKR_API_METHOD
                ,FLICKR_API_KEY
                ,FLICKR_TAG
                ,String.valueOf(FLICKR_COUNT_PER_PAGE)
                ,FLICKR_FORMAT
                ,String.valueOf(FLICKR_NOJSONCALLBACK));

        //Observable для параллельныой загрузки данных с github и Flickr и преобразования данных во вспомогательный класс
        Observable<List<CombinedFilckrAndGithub>> combinedDataObservable = Observable.zip(observable
                , flickrDataModelObservable
                , (dataResponse, flickrDataModel) -> {

                    if (dataResponse.data() != null && flickrDataModel != null)
                    return  combinedData(dataResponse.data().user().repositories().nodes()
                            ,flickrDataModel.getPhotos().getPhoto());
                    else
                        return null;
                });

        if (!disposables.isDisposed())
            disposables.clear();

        iMainView.showLoading();

        disposables.add(combinedDataObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<CombinedFilckrAndGithub>>(){
                    @Override
                    public void onNext(List<CombinedFilckrAndGithub> combinedFilckrAndGithubList) {

                        iMainView.hideLoading();

                        if (combinedFilckrAndGithubList != null)
                            iMainView.showList(combinedFilckrAndGithubList);
                    }

                    @Override
                    public void onError(Throwable e) {

                        iMainView.hideLoading();
                        iMainView.showError();
                    }

                    @Override
                    public void onComplete() {
                        iMainView.hideLoading();
                    }
                }));
    }

    //Заполняем вспомогательный список поочередно, названием репозитория c github и url картинки с Flickr
    private List<CombinedFilckrAndGithub> combinedData(List<GetRepositories.Node> nodeList, List<Photo> photoList){
        List<CombinedFilckrAndGithub> combinedFilckrAndGithubList = new ArrayList<>();

        for (int i = 0; i < nodeList.size(); i++){
            CombinedFilckrAndGithub githubData = new CombinedFilckrAndGithub();
            CombinedFilckrAndGithub flickrData = new CombinedFilckrAndGithub();

            githubData.setValue(nodeList.get(i).name());
            githubData.setIsImageUrl(false);

            flickrData.setValue(buildFlickrImageUrl(photoList.get(i)));
            flickrData.setIsImageUrl(true);

            combinedFilckrAndGithubList.add(githubData);
            combinedFilckrAndGithubList.add(flickrData);
        }

        return combinedFilckrAndGithubList;
    }

    // Формируем url для получения картинки с Flickr, исходя из ранее полученных данных с сервера Flickr
    private String buildFlickrImageUrl(Photo photo){

        return String.format("https://farm%s.staticflickr.com/%s/%s_%s.jpg"
                ,photo.getFarm()
                ,photo.getServer()
                ,photo.getId()
                ,photo.getSecret());
    }

    public void onStop(){
        disposables.clear();
    }
}
